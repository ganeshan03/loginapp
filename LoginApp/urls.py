from django.contrib import admin
from django.urls import path
from dologin import views 
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('',views.loginview,name='login'),
    path('signup/', views.signup, name="signup"),
    path('welcome/',views.welcome, name="welcome"),
    path('admin/', admin.site.urls),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
