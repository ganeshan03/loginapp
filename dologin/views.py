from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login,logout,authenticate
from django.http.response import HttpResponseRedirect
# Create your views here.


def loginview(request):
    form = UserCreationForm()
    context = {
        'form':form
    }

    if request.method == "POST":
        user = request.POST.get('user_name')
        print(user)
        pwd = request.POST.get('your_pass')
        print(pwd)
        userauth = authenticate(username=user, password=pwd)
        login(request,userauth)
        return HttpResponseRedirect('/welcome/')
        
    return render(request,'index.html',context)


def signup(request):
    return render(request,'signup.html',{})


def welcome(request):
    return render(request,'welcome.html',{})


